<div class="page" id="home-page" data-theme="j">
	<header>
		<div class="player-status-container">
			<div class="player-status">
			</div>
		</div>
	</header>
	<div class="main">
		<div class="leader-character-holder">
		</div>
		<div class="main-menu">
			<ul>
				<li><a href="#quest-page">冒険</a></li>
				<li><a href="#edit-page">編成</a></li>
				<li><a href="#supply-page">補給</a></li>
				<li><a href="#enhance-page">強化</a></li>
				<li><a href="#rest-page">休憩</a></li>
				<li><a href="#studio-page">工房</a></li>
			</ul>
		</div>
	</div>
	<footer>
		<ul class="footer-contents">
		</ul>
	</footer>
</div>
<script id="template-player-status" type="text/template">
		<div class="level"><span class="label">Lv.</span><span class="value"><%= level %></span></div>
		<div class="party"><span class="label">部隊数:</span><span class="value"><%= party %></span></div>
		<ul>
			<li　class="resource-food">兵糧:<%= resource1 %></li>
			<li　class="resource-money">矢銭:<%= resource2 %></li>
			<li　class="resource-human">人材:<%= resource3 %></li>
			<li　class="resource-gem">魔石:<%= resource4 %></li>
		</ul>
</script>
<script id="template-footer" type="text/template">
		<li><a href="#home-page">HOME</a></li>
		<li><a href="#edit-page">編成</a></li>
		<li><a href="#supply-page">補給</a></li>
		<li><a href="#enhance-page">強化</a></li>
		<li><a href="#rest-page">休憩</a>/li>
		<li><a href="#studio-page">工房</a></li>
</script>
<script id="template-deck-card" type="text/template">
	<div class="deck-card" data-card-id="<%= card_id %>" data-attribute="<%= attribute %>">
		card_id:<%= card_id %><br />
		level:<%= level %><br />
		attr:<%= attribute %><br />
	</div>
</script>