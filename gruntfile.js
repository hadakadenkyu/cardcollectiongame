module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-typescript');

  grunt.initConfig({
    typescript: {
      base: {
        src: ['./lib/ts/*.ts'],
        dest: './lib/js',
        options: {
          module: 'amd', //or commonjs
          target: 'es5', //or es3
          sourcemap: false,
          declaration: false
        }
      }
    }
  });
  grunt.registerTask("default", ["typescript"]);
};
