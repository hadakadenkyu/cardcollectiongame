<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<?php
	$ua=$_SERVER['HTTP_USER_AGENT'];
	if(strpos($ua,'Android')!==false){
		echo '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,target-densitydpi=160" id="viewport" />';
	} else {
		echo '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" id="viewport" />';
	}
?>
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" href="./lib/css/jqMini.min.css" />
<link rel="stylesheet/less" type="text/css" href="./lib/less/cssa.keyframes.less" />
<link rel="stylesheet/less" type="text/css" href="./lib/less/cssa.less" />
<link rel="stylesheet/less" type="text/css" href="./lib/less/cssa.battle.less" />
<link rel="stylesheet/less" type="text/css" href="./lib/less/cssa.drama.less" />
<link rel="stylesheet/less" type="text/css" href="./css/main.less" />
<title>CardGame Sample</title>
</head>
<body>
<div id="jqmini" class="jqMini">
<?php include_once("home.php"); ?>
</div>
<div id="landscape-alert">
	<p>横向き非対応につき縦向きでご使用ください。</p>
</div>
<!-- library -->
<script src="./lib/js/less-1.3.3.min.js"></script>
<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script src="./lib/js/underscore-min.js"></script>
<script src="./lib/js/backbone-min.js"></script>
<script src="./lib/js/backbone.localStorage-min.js"></script>
<script src="./lib/js/jqMini.js"></script>
<script src="./lib/js/jquery.colorize.js"></script>
<script src="./lib/js/cssa.js"></script>
<script src="./lib/js/uuid.js"></script>
<script src="./lib/js/cssa.node.js"></script>
<script src="./lib/js/cssa.layer.js"></script>
<script src="./lib/js/cssa.sprite.js"></script>
<script src="./lib/js/cssa.sprite_animation.js"></script>
<script src="./lib/js/cssa.effect.js"></script>
<script src="./lib/js/cssa.status.js"></script>
<script src="./lib/js/cssa.unit.js"></script>
<script src="./lib/js/cssa.drama.js"></script>
<script src="./lib/js/cssa.scene.js"></script>
<script src="./lib/js/cssa.actor.js"></script>
<script src="./lib/js/cssa.caption.js"></script>

<!-- backbone -->
<script src="./js/ccg.js"></script>
<script src="./js/model/CardModel.js"></script>
<script src="./js/model/FooterModel.js"></script>
<script src="./js/model/PlayerStatusModel.js"></script>
<script src="./js/collection/CardCollection.js"></script>
<script src="./js/view/FooterView.js"></script>
<script src="./js/view/PlayerStatusView.js"></script>
<script src="./js/view/AppView.js"></script>
<script src="./js/view/BasePageView.js"></script>
<script src="./js/view/HomePageView.js"></script>
<script>
less.optimization = 1;
/* PCの時だけwatchかける */
if(!cssa.isAndroid && !cssa.isIOS && !cssa.isIPad){
	less.watch();
}
$(document).ready(function(){
	CCG.initialize($("#jqmini"))
});
</script>

</body>
</html>