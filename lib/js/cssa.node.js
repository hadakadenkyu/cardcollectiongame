var cssa;
(function (cssa) {
    var Node = (function () {
        function Node() {
            this.position = { x: 0, y: 0 };
            this.parent = null;
            this.children = [];
            this.nodeIndex = cssa.getNextHighestNodeIndex();
            var id = "node-" + this.nodeIndex;
            this.el = Node.baseElem.cloneNode();
            this.$el = $(this.el);
            this.el.id = id;
            this.el.setAttribute('data-node-index', String(this.nodeIndex));
            this.visible = true;
            this.depth = "";
            this.isTouchable = false;
        }
        Object.defineProperty(Node.prototype, "isTouchable", {
            get: function () {
                return this._isTouchable;
            },
            set: function (bool) {
                this.$el.toggleClass("touchable", bool);
                this._isTouchable = bool;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "depth", {
            get: function () {
                return this._depth;
            },
            set: function (z) {
                this._depth = z;
                this.el.style.zIndex = z;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "visible", {
            get: function () {
                return this._visible;
            },
            set: function (bool) {
                this._visible = bool;
                var cary = this.el.className.split(' ');
                var i = cary.indexOf('invisible');
                if (!bool && i === -1) {
                    cary.push('invisible');
                    this.el.className = cary.join(' ');
                } else if (bool && i !== -1) {
                    cary.splice(i, 1);
                    this.el.className = cary.join(' ');
                }
            },
            enumerable: true,
            configurable: true
        });
        Node.prototype.getPosition = function () {
            return this.position;
        };
        Node.prototype.setPosition = function (obj) {
            this.position = obj;
            this.el.style.left = obj.x + 'px';
            this.el.style.top = obj.y + 'px';
            return this;
        };
        Node.prototype.show = function () {
            this.visible = true;
            return this;
        };
        Node.prototype.hide = function () {
            this.visible = false;
            return this;
        };
        Node.prototype.removeSelf = function () {
            if (this.parent) {
                this.parent.removeChild(this);
                return false;
            } else {
                this.$el.remove();
                return this;
            }
        };
        Node.prototype.addChild = function (node) {
            if (!(node instanceof cssa.Node)) {
                console.log(node);
                throw new Error('Nodeの子はNodeでなければならない');
            }
            if (node.parent) {
                node.parent.removeChild(node);
            }
            this.children.push(node);
            node.parent = this;
            this.el.appendChild(node.el);
            return this;
        };
        Node.prototype.removeChild = function (node) {
            var i = this.children.indexOf(node);
            if (i >= 0) {
                this.children.splice(i, 1);
                this.el.removeChild(node.el);
            }
            return this;
        };
        Node.prototype.removeAllChildren = function () {
            var imax = this.children.length;
            for (var i = 0; i < imax; i++) {
                this.children[i].removeSelf();
            }
            return this;
        };
        Node.prototype.removeAllDescendants = function () {
            var imax = this.children.length;
            for (var i = 0; i < imax; i++) {
                this.children[i].removeAllDescendants();
                this.children[i].removeSelf();
            }
            return this;
        };

        Node.prototype.moveTo = function (point, option) {
            var _this = this;
            var dfd = $.Deferred();
            dfd.promise();
            var prop = (typeof (option) == 'object') ? option : { webkitTransitionDuration: 0 };
            var xdiff = point.x - this.getPosition().x;

            var ydiff = point.y - this.getPosition().y;

            if (prop.webkitTransitionDuration == 0 || (xdiff == 0 && ydiff == 0)) {
                this.setPosition(point);
                dfd.resolve();
            } else {
                var resetOption = {};
                prop.webkitTransitionProperty = "-webkit-transform";
                prop.webkitTransform = cssa.getTransformProperty("translate", xdiff + "px," + ydiff + "px");
                for (var i in prop) {
                    resetOption[i] = "";
                }
                var onMoveEndFunc = function (event) {
                    _this.setPosition(point).$el.removeClass("move-to").css(resetOption);
                    cssa.nextTick(dfd.resolve);
                    return false;
                };
                var selector = "#" + this.el.id + ".move-to";
                cssa.$doc.one("webkitTransitionEnd", selector, onMoveEndFunc);
                this.$el.addClass("move-to").css(prop);
                ;
            }
            return dfd.promise();
        };
        Node.prototype.transition = function (name, evt, count) {
            var _this = this;
            var selector = "#" + this.el.id + "[data-transition='" + name + "']";
            var evName = evt ? evt : "webkitAnimationEnd";
            var cnt = count ? count : 1;
            var dfd = $.Deferred();
            var c = 0;
            cssa.$doc.on(evName, selector, function () {
                if (++c >= cnt) {
                    dfd.resolve();
                }
                return false;
            });
            this.el.setAttribute('data-transition', name);
            return dfd.promise().done(function () {
                cssa.$doc.off(evt, selector);
                _this.el.setAttribute('data-transition', '');
            });
        };
        Node.prototype.action = function (name, evt, count) {
            var _this = this;
            var selector = "#" + this.el.id + "[data-action='" + name + "']";
            var evName = evt ? evt : "webkitAnimationEnd";
            var cnt = count ? count : 1;
            var dfd = this.actionDfd = $.Deferred();
            var c = 0;
            cssa.$doc.on(evName, selector, function () {
                if (++c >= cnt) {
                    dfd.resolve();
                }
                return false;
            });
            this.el.setAttribute('data-action', name);
            return dfd.promise().done(function () {
                cssa.$doc.off(evt, selector);
                _this.el.setAttribute('data-action', '');
            });
        };
        Node.prototype.resetAction = function () {
            if (this.actionDfd) {
                this.actionDfd.resolve();
            }
            return this;
        };
        Node.prototype.shake = function (duration, count) {
            var _this = this;
            var dfd = $.Deferred();
            var selector = "#" + this.el.id + ".action-shake";
            cssa.$doc.one("webkitAnimationEnd", selector, function (event) {
                _this.$el.css({ webkitAnimationName: "" }).removeClass("action-shake");
                dfd.resolve();
            });
            this.$el.css({ webkitAnimationName: "kf-shake", webkitAnimationDuration: duration + "ms", webkitAnimationIterationCount: "" + count, webkitAnimationTimingFunction: "linear" }).addClass("action-shake");
            return dfd.promise();
        };
        Node.prototype.zoomTo = function (ratio, option) {
            var _this = this;
            var dfd = $.Deferred();
            var option = (option !== undefined) ? option : { webkitTransitionDuration: 0 };
            option.webkitTransitionProperty = '-webkit-transform';
            option.webkitTransform = cssa.getTransformProperty("scale", ratio + "," + ratio);
            var selector = "#" + this.el.id + ".action-zoom-to";
            cssa.$doc.one("webkitTransitionEnd", selector, function () {
                _this.$el.removeClass("action-zoom-to");
                dfd.resolve();
            });
            this.$el.css(option);
            return dfd.promise();
        };
        Node.prototype.appear = function () {
            var _this = this;
            var dfd = $.Deferred();
            this.$el.css({ webkitTransform: cssa.getTransformProperty("scale", "1.2,1.2") }).show();
            var temp = function (event) {
                _this.$el.removeClass("sprite-appear").css({ webkitTransform: "" });
                return false;
            };
            this.$el.addClass("sprite-appear").one("webkitTransitionEnd", temp).css({ webkitTransform: cssa.getTransformProperty("scale", "1,1") });
            return dfd.promise();
        };
        Node.prototype.fadeIn = function (option) {
            this.$el.css({ opacity: 0 }).show();
            return this.fadeTo(1, option);
        };
        Node.prototype.fadeOut = function (option) {
            return this.fadeTo(0, option);
        };
        Node.prototype.fadeTo = function (opacity, option) {
            var _this = this;
            var stl;
            stl.opacity = opacity;
            stl.webkitTransitionProperty = "opacity";
            stl.webkitTransitionDuration = "250ms";
            stl.webkitTransitionDelay = "0";
            stl.webkitTransitionTimingFunction = "ease-out";
            var css = $.extend(true, stl, option);
            var resetCSS = $.extend(true, {}, css);
            for (var i in resetCSS) {
                resetCSS[i] = "";
            }

            var temp = function (event) {
                _this.$el.css(resetCSS);
                return false;
            };
            this.$el.one("webkitTransitionEnd", temp).css(css);
            return this;
        };
        Node.baseElem = (function () {
            var el = document.createElement('div');
            el.className = 'node';
            return el;
        })();
        return Node;
    })();
    cssa.Node = Node;
})(cssa || (cssa = {}));
