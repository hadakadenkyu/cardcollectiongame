/// <reference path="cssa.sprite.ts" />
module cssa{
	export class Effect extends cssa.Sprite{
		callbackCount:number;
		callbackEvent:string;
		$child:JQuery;
		constructor(){
			super();
			this.el.className += ' effect';
		}
		setup(name){
			// タイプに応じた効果を用意する
			var asset = document.querySelector('.assets .asset[data-name="'+name+'"]');
			this.setAsset(asset);
			this.callbackCount = asset.getAttribute("data-callback-count") ? Number(asset.getAttribute("data-callback-count")):1;
			this.callbackEvent = asset.getAttribute("data-callback-event");
			this.$child = this.$el.children();
			this.visible = false;
			return this;
		}
		register(evt,cnt,func){
			var selector = "#" + this.el.id;
			var count = 0;
			var countFunc = function(){
				if(++count>=cnt){
					cssa.$doc.off(evt,selector,countFunc);
					func();
				}
			}
			cssa.$doc.on(evt,selector,countFunc);
			return this;
		}
		unregister(evt,cnt,func){
			
		}
		invoke(persistent?){
			this.$child.removeClass("invoke");
			var isPersistent = persistent?true:false;
			// 用意されている効果を発動する
			var dfd = $.Deferred();
			var count = 0;
			var selector = "#" + this.el.id;
			// visibleステータスを取得しておく
			var visibility = (this.visible || isPersistent)?true:false;
			var onEndFunc = ()=>{
				if(++count>=this.callbackCount){
					cssa.$doc.off(this.callbackEvent,selector,onEndFunc);
					this.$child.toggleClass("invoke",isPersistent);
					this.visible = visibility;
					dfd.resolve();
				}
			}
			cssa.$doc.on(this.callbackEvent,selector,onEndFunc);
			this.show();
			this.$child.addClass("invoke");
			return dfd.promise();
		}
		revoke(){
			// 効果を消す
			var selector = "#" + this.el.id;
			cssa.$doc.off(this.callbackEvent,selector);
			this.$child.removeClass("invoke");
			return this;
		}
	}
}