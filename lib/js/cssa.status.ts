/// <reference path="cssa.sprite.ts" />
module cssa{
	export class Status extends cssa.Sprite{
		value:number;
		maxValue:number;
		minValue:number;
		low:number;
		private static asset = (function(){
			var asset = document.createElement('div');
			asset.className = 'asset';
			asset.setAttribute('data-role','status');
			asset.innerHTML = '<div class="bar-holder"><div class="bar"></div></div><div class="value"><span class="current-value">0</span>/<span class="max-value">0</span>';
			return asset;
		})();
		constructor(){
			super();
			this.el.className += ' status';
			this.minValue = 0;
			this.low = 0.1;
		}
		setup(obj){
			this.maxValue = obj.maxValue;
			this.minValue = (typeof(obj.minValue) == 'number')?obj.minValue:0;
			this.low = (typeof(obj.low) == 'number')?obj.low:0.1;
			this.setAsset(Status.asset);
			this.$el.find(".max-value").text(this.maxValue);
			this.setValue(obj.value);
		}
		setValue(val:number){
			var val = Math.max(val,this.minValue);
			val = Math.min(val,this.maxValue);
			this.value = val;
			// 小数点第二位までにする
			var w:number = Number((val/this.maxValue).toFixed(2));
			var isLow = Boolean(this.low>=w);
			var prop:any = {};
			if(cssa.isIOS){
				prop.webkitTransform=cssa.getTransformProperty("scale",w+",1");
			} else {
				prop.width=(w*100)+"%";
			}
			this.$el.toggleClass("low",isLow).find(".current-value").text(val);
			this.$el.find(".bar").css(prop);
			return val;
		}
		addValue(val){
			return this.setValue(this.value+val);
		}
	}
}