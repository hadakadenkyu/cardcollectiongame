/// <reference path="cssa.ts" />
interface MSStyleCSSProperties {
    webkitTransform: string;
    webkitTransitionDuration: string;
    webkitMaskImage:string;
    webkitMaskSize:string;
}
interface CSSStyleDeclaration {
    webkitAnimationDuration: string;
    webkitAnimationDelay: string;
    webkitAnimationIterationCount:string;
}
module cssa{
	export class Node{
		position:Point = {x:0,y:0};
		private _visible:boolean;
		private _isTouchable:boolean;
		private _depth;
		parent = null;
		children:any[]=[];
		nodeIndex:number;
		$el:JQuery;
		el:HTMLElement;
		actionDfd:JQueryDeferred;
		private static baseElem:HTMLElement = (function(){
			var el:HTMLElement = document.createElement('div');
			el.className = 'node';
			return el;
		})();
		constructor(){
			this.nodeIndex = cssa.getNextHighestNodeIndex();
			var id = "node-" + this.nodeIndex;
			this.el = <HTMLElement>Node.baseElem.cloneNode();
			this.$el = $(this.el);
			this.el.id = id;
			this.el.setAttribute('data-node-index',String(this.nodeIndex));
			this.visible = true;
			this.depth = "";
			this.isTouchable = false;
		}
		public get isTouchable(){
			return this._isTouchable;
		}
		public set isTouchable(bool){
			this.$el.toggleClass("touchable",bool);
			this._isTouchable = bool;
		}
		public get depth(){
			return this._depth;
		}
		public set depth(z){
			this._depth = z;
			this.el.style.zIndex = z;
		}
		public get visible(){
			return this._visible;
		}
		public set visible(bool){
			this._visible = bool;
			var cary = this.el.className.split(' ');
			var i = cary.indexOf('invisible');
			if(!bool && i === -1){
				cary.push('invisible');
				this.el.className = cary.join(' ');
			} else if(bool && i !== -1){
				cary.splice(i,1);
				this.el.className = cary.join(' ');
			}
		}
		getPosition(){
			return this.position;
		}
		setPosition(obj):Node{
			this.position = obj;
			this.el.style.left = obj.x+'px';
			this.el.style.top = obj.y+'px';
			return this;
		}
		show():Node{
			this.visible = true;
			return this;
		}
		hide():Node{
			this.visible = false;
			return this;
		}
		removeSelf():any{
			if(this.parent){
				this.parent.removeChild(this);
				return false;
			} else {
				this.$el.remove();
				return this;
			}
		}
		addChild(node){
			if(!(node instanceof cssa.Node)){
				console.log(node);
				throw new Error('Nodeの子はNodeでなければならない');
			}
			if(node.parent){
				node.parent.removeChild(node);
			}
			this.children.push(node);
			node.parent = this;
			this.el.appendChild(node.el);
			return this;
		}
		removeChild(node){
			var i = this.children.indexOf(node);
			if(i>=0){
				this.children.splice(i,1);
				this.el.removeChild(node.el);
			}
			return this;
		}
		removeAllChildren(){
			var imax = this.children.length;
			for(var i=0;i<imax;i++){
				this.children[i].removeSelf();
			}
			return this;
		}
		removeAllDescendants(){
			var imax = this.children.length;
			for(var i=0;i<imax;i++){
				this.children[i].removeAllDescendants();
				this.children[i].removeSelf();
			}
			return this;
		}
		// point = {x:0,y:0},duration in ms
		moveTo(point:Point,option){
			var dfd = $.Deferred();
			dfd.promise();
			var prop = (typeof(option) == 'object')?option:{webkitTransitionDuration:0};
			var xdiff = point.x - this.getPosition().x;
			//xdiff *= -1;
			var ydiff = point.y - this.getPosition().y;
			//ydiff *= -1;
			// trigger callback if duration == 0 OR no movement
			if(prop.webkitTransitionDuration == 0 || (xdiff==0&&ydiff==0)){
				this.setPosition(point);
				dfd.resolve();
			}else{
				var resetOption = {};
				prop.webkitTransitionProperty="-webkit-transform";
				prop.webkitTransform=cssa.getTransformProperty("translate",xdiff+"px,"+ydiff+"px");
				for(var i in prop){
					resetOption[i] = "";
				}
				var onMoveEndFunc = (event)=>{
					this.setPosition(point).$el.removeClass("move-to").css(resetOption);
					cssa.nextTick(dfd.resolve);
					return false;
				};
				var selector = "#" + this.el.id + ".move-to";
				cssa.$doc.one("webkitTransitionEnd",selector,onMoveEndFunc);
				this.$el.addClass("move-to").css(prop);;
			}
			return dfd.promise();
		}
		transition(name:string,evt,count){
			var selector = "#"+this.el.id + "[data-transition='"+name+"']";
			var evName = evt?evt:"webkitAnimationEnd";
			var cnt = count?count:1;
			var dfd = $.Deferred();
			var c = 0;
			cssa.$doc.on(evName,selector,function(){
				if(++c>=cnt){
					dfd.resolve();
				}
				return false;
			});
            this.el.setAttribute('data-transition', name);
			return dfd.promise().done(()=>{
				cssa.$doc.off(evt,selector);
	            this.el.setAttribute('data-transition', '');
			});
		}
		action(name:string,evt?,count?){
			var selector = "#"+this.el.id + "[data-action='"+name+"']";
			var evName = evt?evt:"webkitAnimationEnd";
			var cnt = count?count:1;
			var dfd = this.actionDfd = $.Deferred();
			var c = 0;
			cssa.$doc.on(evName,selector,function(){
				if(++c>=cnt){
					dfd.resolve();
				}
				return false;
			});
            this.el.setAttribute('data-action', name);
			return dfd.promise().done(()=>{
				cssa.$doc.off(evt,selector);
	            this.el.setAttribute('data-action', '');
			});
		}
		resetAction(){
			if(this.actionDfd){
				this.actionDfd.resolve();
			}
			return this;
		}
		shake(duration,count){
			// 震わせるファンクション。
			var dfd = $.Deferred();
			var selector = "#" + this.el.id + ".action-shake";
			cssa.$doc.one("webkitAnimationEnd",selector,(event)=>{
				this.$el.css({webkitAnimationName:""}).removeClass("action-shake");
				dfd.resolve();
			});
			this.$el.css({webkitAnimationName:"kf-shake",webkitAnimationDuration:duration+"ms",webkitAnimationIterationCount:""+count,webkitAnimationTimingFunction:"linear"}).addClass("action-shake");
			return dfd.promise();
		}
		zoomTo(ratio,option){
			var dfd = $.Deferred();
			var option = (option!==undefined)?option:{webkitTransitionDuration:0};
			option.webkitTransitionProperty = '-webkit-transform';
			option.webkitTransform = cssa.getTransformProperty("scale",ratio+","+ratio);
			var selector = "#" + this.el.id + ".action-zoom-to";
			cssa.$doc.one("webkitTransitionEnd",selector,()=>{
				this.$el.removeClass("action-zoom-to");
				dfd.resolve();
			});
			this.$el.css(option);
			return dfd.promise();
		}
		appear(){
			// 登場
			var dfd = $.Deferred();
			this.$el.css({webkitTransform:cssa.getTransformProperty("scale","1.2,1.2")}).show();
			var temp = (event)=>{
				this.$el.removeClass("sprite-appear").css({webkitTransform:""});
				return false;
			}
			this.$el.addClass("sprite-appear").one("webkitTransitionEnd",temp).css({webkitTransform:cssa.getTransformProperty("scale","1,1")});
			return dfd.promise();
		}
		fadeIn(option){
			this.$el.css({opacity:0}).show();
			return this.fadeTo(1,option);
		}
		fadeOut(option){
			return this.fadeTo(0,option);
		}
		fadeTo(opacity,option){
			var stl:{
				opacity:number;
				webkitTransitionProperty:string;
				webkitTransitionDuration:string;
				webkitTransitionDelay:string;
				webkitTransitionTimingFunction:string;
			};
			stl.opacity = opacity;
			stl.webkitTransitionProperty = "opacity";
			stl.webkitTransitionDuration = "250ms";
			stl.webkitTransitionDelay = "0";
			stl.webkitTransitionTimingFunction = "ease-out";
			var css = $.extend(true,stl,option);
			var resetCSS = $.extend(true,{},css);
			for(var i in resetCSS){
				resetCSS[i]="";
			}
			// 登場
			var temp = (event)=>{
				this.$el.css(resetCSS);
				return false;
			}
			this.$el.one("webkitTransitionEnd",temp).css(css);
			return this;
		}
	}
}