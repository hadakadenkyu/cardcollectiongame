var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Sprite = (function (_super) {
        __extends(Sprite, _super);
        function Sprite() {
            _super.call(this);
            this.size = { width: 0, height: 0 };
            this.anchorPoint = { x: 0, y: 0 };
            this.el.className += ' sprite';
        }
        Sprite.prototype.setAsset = function (element) {
            if (element instanceof jQuery) {
                element = $(element).eq(0).get(0);
            }
            var asset = this.asset = element.cloneNode(true);
            var $asset = this.$asset = $(asset);

            this.el.appendChild(asset);
            var cstl = getComputedStyle(element);
            var w = parseInt(cstl.width);
            var h = parseInt(cstl.height);

            if (!w || !h) {
                var vis = element.style.visibility;
                var dpl = element.style.dislay;
                var pos = element.style.position;
                element.style.visibility = 'hidden';
                element.style.display = 'block';
                element.style.position = 'absolute';
                cstl = getComputedStyle(element);
                w = parseInt(cstl.width);
                w = w ? w : 0;
                h = parseInt(cstl.height);
                h = h ? h : 0;
                element.style.visibility = vis;
                element.style.display = dpl;
                element.style.position = pos;
            }

            this.setSize({ width: w, height: h });
            return this;
        };
        Sprite.prototype.getAnchorPoint = function () {
            return this.anchorPoint;
        };
        Sprite.prototype.setAnchorPoint = function (point) {
            var xvalue = Math.floor(point.x);
            var xunit = "px";
            var xmatch = String(point.x).match(/[0-9]+([^0-9]+)$/);
            if (xmatch) {
                xunit = xmatch[1];
            }
            var yvalue = Math.floor(point.y);
            var yunit = "px";
            var ymatch = String(point.y).match(/[0-9]+([^0-9]+)$/);
            if (ymatch) {
                point;
                yunit = ymatch[1];
            }
            xvalue *= -1;
            yvalue *= -1;
            var left = xvalue + xunit;
            var top = yvalue + yunit;

            this.el.style.marginLeft = left;
            this.el.style.marginTop = top;
            this.anchorPoint = point;
            return this;
        };
        Sprite.prototype.getSize = function () {
            return this.size;
        };
        Sprite.prototype.setSize = function (size) {
            this.size = size;
            this.el.style.width = size.width + 'px';
            this.el.style.height = size.height + 'px';
            this.setAnchorPoint({ x: size.width / 2, y: size.height / 2 });
            return this;
        };
        return Sprite;
    })(cssa.Node);
    cssa.Sprite = Sprite;
})(cssa || (cssa = {}));
