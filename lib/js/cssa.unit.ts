/// <reference path="cssa.sprite.ts" />
/// <reference path="cssa.status.ts" />
module cssa{
	export class Unit extends cssa.Sprite{
		hitPoint;
		src:string;
		$damageEffect;
		$defeatEffect;
		$purifyEffect;
		constructor(){
			super();
			this.el.className += ' unit';
		}
		setup(name){
			var orig_asset = document.querySelector('.assets .asset[data-name="'+name+'"]');
			// HPを設定
			this.hitPoint = new cssa.Status();
			var hp = orig_asset.getAttribute("data-hit-point")?Number(orig_asset.getAttribute("data-hit-point")):100;
			var maxHp = orig_asset.getAttribute("data-max-hit-point")?Number(orig_asset.getAttribute("data-max-hit-point")):100;
			var low = orig_asset.getAttribute("data-low")?Number(orig_asset.getAttribute("data-low")):0.1;
			this.hitPoint.setup({value:hp,maxValue:maxHp,low:low,name:"HP"});
			this.src = orig_asset.getAttribute("data-src")?orig_asset.getAttribute("data-src"):"";
			var img = new Image();
			var cstl = getComputedStyle(orig_asset);
			img.width = parseInt(cstl.width);
			img.height = parseInt(cstl.height);
			img.src = this.src;
			// ダメージエフェクト用要素生成
			var daef = document.createElement("div");
			daef.className = 'damage-effect';
			var mskd = document.createElement("div");
			mskd.style.webkitMaskImage = 'url("'+this.src+'")';
			mskd.style.webkitMaskSize = 'contain';
			daef.appendChild(mskd);

			// 倒されエフェクトと浄化エフェクト用要素をclone
			var deef = <HTMLElement>daef.cloneNode(true);
			deef.className = 'defeat-effect';
			var puef = <HTMLElement>daef.cloneNode(true);
			puef.className = 'purify-effect';

			this.$damageEffect = $(daef);
			this.$defeatEffect = $(deef);
			this.$purifyEffect = $(puef);
			this.setAsset(orig_asset);
			// 後から足すと面倒がない
            this.$el.find('.asset').append(img,daef,deef,puef);
			return this;
		}
		setupCut(){
			var w = this.size.width;
			var h = this.size.height;
			var src = this.$el.find("img").attr('src');
			var $el = $("<div class='body-parts'><div class='upper-body'></div><div class='lower-body'></div></div>");
			$el.find(".upper-body,.lower-body").css({width:w+"px",height:h+"px",backgroundImage:"url("+src+")"});
			this.$el.children(".asset").append($el);
			return this;
		}
		cut(){
			var dfd = $.Deferred();
			var bdy = this.$el.find(".body-parts");
			// モンスターが打ち倒される
			var cnt = 0;
			var onEndFunc = function(event){
				cnt++;
				if(cnt>=3){
					cssa.$doc.off("webkitTransitionEnd",selector,onEndFunc);
					dfd.resolve();
				}
				return false;
			};
			// モンスターイメージ消す
			this.$el.find("img").hide();
			var selector = "#" + this.el.id + " .body-parts";
			cssa.$doc.on("webkitTransitionEnd",selector,onEndFunc);
			bdy.addClass("invoke");
			return dfd.promise();
		}
		flash(duration,callback){
			// モンスターを光らせる。
		}
		damage(damagePoint){
			var dfd = $.Deferred();
			var dp = (damagePoint === undefined)?0:damagePoint*-1;
			// モンスターがダメージを負う
			var onEndFunc = (event)=>{
				this.$damageEffect.removeClass("invoke");
				dfd.resolve();
				return false;
			};
			this.$damageEffect.removeClass("invoke");
			var selector = "#" + this.el.id + " .damage-effect";
			cssa.$doc.one("webkitTransitionEnd",selector,onEndFunc);
			this.$damageEffect.addClass("invoke");
			this.hitPoint.addValue(dp);
			return dfd.promise();
		}
		defeat(){
			var dfd = $.Deferred();
			// モンスターが打ち倒される
			var onEndFunc = (event)=>{
				this.$defeatEffect.hide();
				dfd.resolve();
				return false;
			};
			// モンスターイメージ消す
			this.$el.find("img").hide();
			var selector = "#" + this.el.id + " .defeat-effect";
			cssa.$doc.one("webkitTransitionEnd",selector,onEndFunc);
			this.$defeatEffect.addClass("invoke");
			return dfd.promise();
		}
		purify(){
			var dfd = $.Deferred();
			// モンスターが浄化される
			var onEndFunc = (event)=>{
				this.$purifyEffect.hide().removeClass("invoke");
				dfd.resolve();
				return false;
			};
			// モンスターイメージ消す
			this.$el.find(".monster-image").hide();
			var selector = "#" + this.el.id + " .purify-effect";
			cssa.$doc.one("webkitTransitionEnd",selector,onEndFunc);
			// 薄くなりながら上に消える
			this.$purifyEffect.addClass("invoke");
			return dfd.promise();
		}
		charge(targetPos,func){
			var origPos = {x:this.position.x,y:this.position.y};
			var func = func?func:Function;
			return this.moveTo(targetPos,{webkitTransitionDuration:'250ms'}).then(()=>{
				func();
				return this.moveTo(origPos,{webkitTransitionDuration:'250ms'})
			});
			/*
			var dfd = $.Deferred();
			var id = this.$el.attr("id");
			var selector = "#"+id;
			// 座標までの移動量計算
			var origPos = {x:this.position.x,y:this.position.y};
			var xdiff = targetPos.x - this.getPosition().x;
			var ydiff = targetPos.y - this.getPosition().y;
			var func = func?func:Function;
			//var count = 0;
			var $childEl = this.$el.children(".asset");
			var childEl = $childEl.get(0);
			var onChargeHitFunc = function(event){
				// attackの何かを起動する
				func();
				// debug console.log("chargeMiddle:temp1:id="+id);
				$childEl.one('webkitTransitionEnd',onChargeEndFunc);
				// 元の位置に戻す
				childEl.style.webkitTransform = cssa.getTransformProperty("translate",0,0);
				return false;
			}
			var onChargeEndFunc = function(event){
				// debug console.log("chargeEnd:temp2:id="+id);
				$childEl.removeClass("charge");
				dfd.resolve();
				return false;
			};
			$childEl.one('webkitTransitionEnd',onChargeHitFunc).addClass("charge");
			childEl.style.webkitTransform = cssa.getTransformProperty("translate",xdiff+"px,"+ydiff+"px");
			// debug console.log("charge:afterBind");
			return dfd.promise();
			*/
		}
	}
}