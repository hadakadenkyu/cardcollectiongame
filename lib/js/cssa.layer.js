var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Layer = (function (_super) {
        __extends(Layer, _super);
        function Layer() {
            _super.call(this);
            this.size = { width: 0, height: 0 };
            this.el.className += ' layer';
            this.viewPoint = { x: 0, y: 0 };
        }
        Layer.prototype.getSize = function () {
            return this.size;
        };
        Layer.prototype.setSize = function (size) {
            this.size = size;
            this.el.style.width = size.width + "px";
            this.el.style.height = size.height + "px";
            return this;
        };
        Layer.prototype.setViewPoint = function (cood) {
            this.viewPoint = cood;
            var target = {
                x: 0 - cood.x,
                y: 0 - cood.y
            };
            if (cssa.isIOS) {
                this.el.style.webkitTransform = cssa.getTransformProperty("translate", target.x + "px," + target.y + "px");
            } else {
                this.el.style.left = target.x + "px";
                this.el.style.top = target.y + "px";
            }
        };
        Layer.prototype.moveViewPoint = function (cood, duration) {
            if (typeof duration === "undefined") { duration = 0; }
            var _this = this;
            var dfd = $.Deferred();
            var xdiff = this.viewPoint.x - cood.x;
            var ydiff = this.viewPoint.y - cood.y;
            if ((xdiff == 0 && ydiff == 0) || duration == 0) {
                this.setViewPoint(cood);
                dfd.resolve();
            } else {
                var selector = "#" + this.el.id;
                var onEndFunc = function (event) {
                    if (event.target == _this.el) {
                        cssa.$doc.off("webkitTransitionEnd left top", selector, onEndFunc);
                        dfd.resolve();
                    }
                };
                cssa.$doc.on("webkitTransitionEnd left top", selector, onEndFunc);
                this.el.style.webkitTransitionDuration = duration + "ms";
                this.setViewPoint(cood);
            }
            return dfd.promise();
        };
        return Layer;
    })(cssa.Node);
    cssa.Layer = Layer;
})(cssa || (cssa = {}));
