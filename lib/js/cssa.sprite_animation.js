var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var SpriteAnimation = (function (_super) {
        __extends(SpriteAnimation, _super);
        function SpriteAnimation() {
            _super.call(this);
            this.el.className += ' sprite-animation';
        }
        SpriteAnimation.prototype.setup = function (name) {
            var asset = document.querySelector('.assets .asset[data-name="' + name + '"]');
            this.setAsset(asset);
            var bd;
            var w = this.size.width;
            var h = this.size.height;
            var cptl = getComputedStyle(asset);
            var settings = {};
            settings.duration = Math.round(parseFloat(cptl.webkitAnimationDuration) * 1000);
            settings.delay = Math.round(parseFloat(cptl.webkitAnimationDelay) * 1000);
            settings.iterationCount = cptl.webkitAnimationIterationCount;
            var sprite_size = cptl.backgroundSize.split(" ");
            var sprite_width = parseInt(sprite_size[0]);
            var sprite_height = parseInt(sprite_size[1]);
            var is_horizontal = Boolean(sprite_width > w);
            if (is_horizontal) {
                settings.frames = ~~(sprite_width / w);
                bd = w;
            } else {
                settings.frames = ~~(sprite_height / h);
                bd = h;
            }
            this.settings = settings;
            settings.id = asset.getAttribute("data-id");
            if (settings.id === null) {
                settings.id = "sprite-animation-" + Date.now();
            }
            this.el.id = settings.id;
            if (!document.getElementById("style-" + name)) {
                var styleText = "";
                styleText += "[data-name='" + name + "'].invoke {";
                styleText += "-webkit-animation-name:kf-" + name + ";";
                styleText += "}";
                var tick = 0.0001;
                var style = document.createElement("style");
                style.id = "style-" + name;
                var imax = settings.frames;
                var i = 0;
                var frame = 0;
                styleText += "@-webkit-keyframes kf-" + name + " {";
                for (i; i < imax; i++) {
                    var fromPercent = (i == 0) ? "from" : (i / imax * 100).toFixed(4) + "%";
                    var toPercent = (i == (imax - 1)) ? "to" : ((i + 1) / imax * 100 - tick).toFixed(4) + "%";
                    var dist = frame * bd * -1;
                    styleText += fromPercent + "," + toPercent + " {background-position:";
                    styleText += (is_horizontal) ? dist + "px 0}" : "0 " + dist + "px}";
                    frame++;
                }
                styleText += "}";
                style.appendChild(document.createTextNode(styleText));
                document.getElementsByTagName('head')[0].appendChild(style);
                console.log(styleText);
            }
            return this;
        };
        SpriteAnimation.prototype.invoke = function () {
            var _this = this;
            var selector = "#" + this.el.id;
            var dfd = $.Deferred();
            var count = 0;
            var visibility = this.visible;
            if (this.settings.iterationCount != 'infinite') {
                var onEndFunc = function () {
                    _this.$asset.removeClass("invoke");
                    _this.visible = visibility;
                    cssa.nextTick(dfd.resolve);
                };
                cssa.$doc.one('webkitAnimationEnd', selector, onEndFunc);
            }
            this.show();
            this.$asset.addClass("invoke");
            return dfd.promise();
        };
        SpriteAnimation.prototype.revoke = function () {
            this.$asset.removeClass("invoke");
        };
        return SpriteAnimation;
    })(cssa.Sprite);
    cssa.SpriteAnimation = SpriteAnimation;
})(cssa || (cssa = {}));
