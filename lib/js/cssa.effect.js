var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Effect = (function (_super) {
        __extends(Effect, _super);
        function Effect() {
            _super.call(this);
            this.el.className += ' effect';
        }
        Effect.prototype.setup = function (name) {
            var asset = document.querySelector('.assets .asset[data-name="' + name + '"]');
            this.setAsset(asset);
            this.callbackCount = asset.getAttribute("data-callback-count") ? Number(asset.getAttribute("data-callback-count")) : 1;
            this.callbackEvent = asset.getAttribute("data-callback-event");
            this.$child = this.$el.children();
            this.visible = false;
            return this;
        };
        Effect.prototype.register = function (evt, cnt, func) {
            var selector = "#" + this.el.id;
            var count = 0;
            var countFunc = function () {
                if (++count >= cnt) {
                    cssa.$doc.off(evt, selector, countFunc);
                    func();
                }
            };
            cssa.$doc.on(evt, selector, countFunc);
            return this;
        };
        Effect.prototype.unregister = function (evt, cnt, func) {
        };
        Effect.prototype.invoke = function (persistent) {
            var _this = this;
            this.$child.removeClass("invoke");
            var isPersistent = persistent ? true : false;

            var dfd = $.Deferred();
            var count = 0;
            var selector = "#" + this.el.id;

            var visibility = (this.visible || isPersistent) ? true : false;
            var onEndFunc = function () {
                if (++count >= _this.callbackCount) {
                    cssa.$doc.off(_this.callbackEvent, selector, onEndFunc);
                    _this.$child.toggleClass("invoke", isPersistent);
                    _this.visible = visibility;
                    dfd.resolve();
                }
            };
            cssa.$doc.on(this.callbackEvent, selector, onEndFunc);
            this.show();
            this.$child.addClass("invoke");
            return dfd.promise();
        };
        Effect.prototype.revoke = function () {
            var selector = "#" + this.el.id;
            cssa.$doc.off(this.callbackEvent, selector);
            this.$child.removeClass("invoke");
            return this;
        };
        return Effect;
    })(cssa.Sprite);
    cssa.Effect = Effect;
})(cssa || (cssa = {}));
