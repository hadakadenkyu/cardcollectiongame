/// <reference path="cssa.node.ts" />
module cssa{
	/* スプライト。サイズを持ち、setAssetで中身のhtmlをセット出来る */
	export class Sprite extends cssa.Node{
		size:Size = {width:0,height:0};
		anchorPoint:Point;
		asset:any;
		$asset:any;
		constructor(){
			super();
			this.anchorPoint = {x:0,y:0};
			this.el.className += ' sprite';
		}
		setAsset(element){
			// jQueryオブジェクトだったらelementを取り出す
			if(element instanceof jQuery){
				element = $(element).eq(0).get(0);
			}
			var asset = this.asset = element.cloneNode(true);
			var $asset = this.$asset = $(asset);
			// 受け取った要素を突っ込む
			this.el.appendChild(asset);
            var cstl = getComputedStyle(element);
            var w = parseInt(cstl.width);
            var h = parseInt(cstl.height);
            // 値が取得できなかった場合（autoとか、0とか）は念のためdisplayを入れる
            if(!w || !h){
                var vis = element.style.visibility;
                var dpl = element.style.dislay;
                var pos = element.style.position;
                element.style.visibility='hidden';
                element.style.display='block';
                element.style.position='absolute';
                cstl =getComputedStyle(element);
                w = parseInt(cstl.width);
                w=w?w:0;
                h = parseInt(cstl.height);
                h=h?h:0;
                element.style.visibility=vis;
                element.style.display=dpl;
                element.style.position=pos;
            }
			// サイズをセットし中心をズラす
			this.setSize({width:w,height:h});
			return this;
		}
		getAnchorPoint(){
			return this.anchorPoint;
		}
		setAnchorPoint(point:Point){
			var xvalue = Math.floor(point.x);
			var xunit = "px";
			var xmatch = String(point.x).match(/[0-9]+([^0-9]+)$/);
			if(xmatch){
				xunit = xmatch[1];
			}
			var yvalue = Math.floor(point.y);
			var yunit = "px";
			var ymatch = String(point.y).match(/[0-9]+([^0-9]+)$/);
			if(ymatch){point
				yunit = ymatch[1];
			}
			xvalue *= -1;
			yvalue *= -1;
			var left = xvalue+xunit;
			var top = yvalue+yunit;
			
			this.el.style.marginLeft = left;
			this.el.style.marginTop = top;
			this.anchorPoint = point;
			return this;
		}
		getSize(){
			return this.size;
		}
		setSize(size:Size){
			this.size = size;
			this.el.style.width = size.width+'px';
			this.el.style.height = size.height+'px';
			this.setAnchorPoint({x:size.width/2,y:size.height/2});
			return this;
		}
	}
}