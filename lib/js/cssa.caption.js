var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Caption = (function (_super) {
        __extends(Caption, _super);
        function Caption() {
            _super.call(this);
            this.el.className += ' caption';
            var div = document.createElement('div');
            div.className = 'title-plate';
            var p = document.createElement('p');
            this.el.appendChild(div);
            this.el.appendChild(p);
            this.titleHolder = div;
            this.textHolder = p;
        }
        Caption.prototype.setTitle = function (title) {
            this.title = title;
            this.titleHolder.innerHTML = title;
        };
        Caption.prototype.setText = function (text) {
            this.text = text;
            this.textHolder.innerHTML = text;
        };
        return Caption;
    })(cssa.Sprite);
    cssa.Caption = Caption;
})(cssa || (cssa = {}));
