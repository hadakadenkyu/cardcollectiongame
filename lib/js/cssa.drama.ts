/// <reference path="cssa.layer.ts" />
/// <reference path="cssa.caption.ts" />
/// <reference path="cssa.scene.ts" />
/// <reference path="cssa.actor.ts" />
/// <reference path="cssa.effect.ts" />

module cssa{
	export class Drama extends cssa.Layer{
		scene;
		caption;
		captionLayer;
		effectLayer;
		transitionLayer;
		casts;
		currentScripts;
		textIndex:number;
		sceneIndex:number;
		settings;
		currentDeferreds;
		wait:number;
		constructor(){
			super();
			this.el.className += ' drama';
			
			this.scene = new cssa.Scene();
			this.addChild(this.scene);

			var captionLayer = new cssa.Layer();
			this.addChild(captionLayer);
			this.captionLayer = captionLayer;
			captionLayer.el.className += ' caption-layer';
			var caption = new cssa.Caption();
			this.caption = caption;
			captionLayer.addChild(caption);

			this.effectLayer = new cssa.Layer();
			this.effectLayer.el.className += ' effect-layer';
			this.addChild(this.effectLayer);

			this.transitionLayer = new cssa.Layer();
			this.transitionLayer.el.className += ' transition-layer';
			this.addChild(this.transitionLayer);

			// 役者が入る
			this.casts = {};
			// 原稿が入る
			this.currentScripts = [];
			this.textIndex = 0;
			this.sceneIndex = 0;
		}
		setup(settings:any){
			this.settings = settings;
			this.setSize(settings.screenSize);
			this.scene.setSize(settings.screenSize);
			this.scene.bg.setSize(settings.screenSize);
			this.scene.characterLayer.setSize(settings.screenSize);
			this.captionLayer.setSize(settings.screenSize);
			this.effectLayer.setSize(settings.screenSize);
			this.transitionLayer.setSize(settings.screenSize);
			this.caption.setSize(settings.captionSize);
			this.caption.setPosition(settings.captionPosition);

			// シーン設定
			this.scene.setup(settings.scenes);
			this.changeScene(0);
			
			return this;
		}
		next(){
			if($.when.apply(this,this.currentDeferreds).state() == 'resolved'){
				if(!this.nextText()){
					return this.nextScene();
				}
				return true;
			};
			return true;
		}
		nextText(){
			if(this.textIndex<(this.currentScripts.length-1)){
				this.textIndex++;
				var txt = this.currentScripts[this.textIndex];
				this.caption.setText(txt);
				this.currentDeferreds.push(cssa.waitFor(this.wait));
				return true;
			} else {
				return false
			}
		}
		prevText(){
			return this;
		}
		changeScene(sceneIndex){
			this.sceneIndex = sceneIndex;
			this.currentDeferreds = [];
			this.textIndex = 0;
			var currentScene = this.settings.scenes[this.sceneIndex];
			this.currentScripts = currentScene.scripts;
			var txt = this.currentScripts[this.textIndex];
			this.caption.setTitle(currentScene.title);
			this.caption.setText(this.currentScripts[this.textIndex]);
			this.wait = 100;
			this.currentDeferreds.push(cssa.waitFor(this.wait));

			// シーントランジション
			if(currentScene.transition){
				var def = this.transitionLayer.transition(currentScene.transition,'webkitAnimationEnd');
				this.currentDeferreds.push(def);
			}
			// シーンエフェクト
			if(currentScene.effect){
				var effect = new cssa.Effect();
				effect.setup(currentScene.effect);
				var pos:Point = {
					x:this.size.width/2,
					y:this.size.height/2
				};
				effect.setPosition(pos);
				this.effectLayer.addChild(effect);
				effect.invoke();
			}
			// シーン切り換え
			this.scene.change(currentScene);

			// Deferreds配列合成
			Array.prototype.push.apply(this.currentDeferreds,this.scene.currentDeferreds);
			return this;
		}
		nextScene(){
			if(this.sceneIndex>=this.settings.scenes.length-1){
				return false;
			} else{
				this.sceneIndex++;
				this.changeScene(this.sceneIndex);
				return true;
			}
		}
		prevScene(){
			this.sceneIndex--;
			this.textIndex = 0;
			return this;
		}
	}
}