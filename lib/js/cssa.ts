/// <reference path="../../d.ts/DefinitelyTyped/jquery/jquery.d.ts" />
declare var process:any;
interface Point{
	x:number;
	y:number;
}
interface Size{
	width:number;
	height:number;
}
module cssa{
	export var getNextHighestNodeIndex = (function () { var i = 0; return function () { return i++; } })()
	export var $doc = $(document);
	export var mobileDevice : {
		os:string;
		version:string;
		majorVersion:number;
		minorVersion:number;
	};
	export var isAndroid:boolean;
	export var isIOS:boolean;
	export var isIPad:boolean;
	export var getTransformProperty;
	(function(){
		var transformFunc = function(property, args) {
			var prop = property;
			switch(property){
				case "scale":
				case "translate":
				case "rotate":
					prop += "("+args+")";
				break;
				default:
					prop = "";
			}
			return prop;
		};
		var iosTransformFunc = function(property, args) {
			var prop = property;
			switch(property){
				case "scale":
					prop += "3d("+args+",1)";
					break;
				case "translate":
					prop += "3d("+args+",0)";
					break;
				case "rotate":
					prop += "3d(0,0,1,"+args+")";
					break;
				default:
					prop = "";
			}
			return prop;
		};
		cssa.isAndroid = Boolean(navigator.userAgent.indexOf("Android")!=-1);
		cssa.isIOS = Boolean(navigator.userAgent.indexOf("iPhone OS")!=-1);
		cssa.isIPad = Boolean(navigator.userAgent.indexOf("iPad")!=-1);
		cssa.mobileDevice = {
			os:"",
			version:"0",
			majorVersion:0,
			minorVersion:0,
		}
		var reg;
		if(cssa.isAndroid){
			cssa.mobileDevice.os = "Android";
			reg = new RegExp('Android ([0-9\.]+)');
		} else if (cssa.isIOS){
			cssa.mobileDevice.os = "iOS";
			reg = new RegExp('iPhone OS ([0-9\.]+)');
		}
		if(cssa.mobileDevice.os !== ""){
			cssa.mobileDevice.version = navigator.userAgent.match(reg)[1];
			var versionArray = cssa.mobileDevice.version.split(".");
			if(versionArray.length>0){
				cssa.mobileDevice.majorVersion = Number(versionArray[0]);
			}
			if(versionArray.length>1){
				cssa.mobileDevice.minorVersion = Number(versionArray[1]);
			}
		}
		cssa.getTransformProperty = cssa.isIOS ? iosTransformFunc : transformFunc;
	})();
	export function waitFor(ms:number){
		var dfd = new $.Deferred();
		setTimeout(function(){
			dfd.resolve();
		}, ms);
		return dfd.promise();
	}
	export var nextTick:any;
	if (typeof(window.setImmediate) === 'function') {
		nextTick = window.setImmediate;
	} else if (typeof(process) === 'object' && typeof(process.nextTick) === 'function') {
		nextTick = process.nextTick;
	} else {
		nextTick = function (fn) {
			setTimeout(fn, 0);
		}
	}
}