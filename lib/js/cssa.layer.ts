/// <reference path="cssa.node.ts" />
module cssa{
	/* レイヤー。サイズを持ち、視点移動ができる */
	export class Layer extends cssa.Node{
		viewPoint:Point;
		size:Size = {width:0,height:0};
		constructor(){
			super();
			this.el.className += ' layer';
			this.viewPoint = {x:0,y:0};
		}
		getSize(){
			return this.size;
		}
		setSize(size:Size){
			this.size = size;
			this.el.style.width = size.width+"px";
			this.el.style.height = size.height+"px";
			return this;
		}
		setViewPoint(cood:Point){
			this.viewPoint = cood;
			var target:Point = {
				x:0-cood.x,
				y:0-cood.y
			};
			if(cssa.isIOS){
				this.el.style.webkitTransform = cssa.getTransformProperty("translate",target.x+"px,"+target.y+"px");
			} else {
				this.el.style.left = target.x + "px";
				this.el.style.top = target.y + "px";
			}
		}
		moveViewPoint(cood:Point,duration:number=0){
			var dfd = $.Deferred();
			var xdiff = this.viewPoint.x - cood.x;
			var ydiff = this.viewPoint.y -cood.y;
			if((xdiff==0&&ydiff==0) || duration==0){
				this.setViewPoint(cood);
				dfd.resolve();
			} else {
				var selector = "#"+this.el.id;
				var onEndFunc = (event)=>{
					if(event.target == this.el){
						cssa.$doc.off("webkitTransitionEnd left top",selector,onEndFunc);
						dfd.resolve();
					}
				}
				cssa.$doc.on("webkitTransitionEnd left top",selector,onEndFunc);
				this.el.style.webkitTransitionDuration=duration+"ms";
				this.setViewPoint(cood);
			}
			return dfd.promise();
		}
	}
}