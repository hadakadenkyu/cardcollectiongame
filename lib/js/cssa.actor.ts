/// <reference path="cssa.sprite.ts" />
module cssa{
	export class Actor extends cssa.Sprite{
		actor_id:string;
		constructor(){
			super();
			this.el.className += ' actor';
			this.actor_id = '';
			this.visible = false;
		}
		setup(name){
			var asset = document.querySelector('.assets .asset[data-name="'+name+'"]');
			this.setAsset(asset);
			this.actor_id = name;
			this.$el.removeAttr('data-transition').removeAttr('data-action');
			return this;
		}
		// 有効無効チェンジ
		activate(){
			this.visible = true;
			this.$el.addClass('active');
			return this;
		}
		deactivate(){
			this.$el.removeClass('active');
			return this;
		}
	}
}