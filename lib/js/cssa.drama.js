var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Drama = (function (_super) {
        __extends(Drama, _super);
        function Drama() {
            _super.call(this);
            this.el.className += ' drama';

            this.scene = new cssa.Scene();
            this.addChild(this.scene);

            var captionLayer = new cssa.Layer();
            this.addChild(captionLayer);
            this.captionLayer = captionLayer;
            captionLayer.el.className += ' caption-layer';
            var caption = new cssa.Caption();
            this.caption = caption;
            captionLayer.addChild(caption);

            this.effectLayer = new cssa.Layer();
            this.effectLayer.el.className += ' effect-layer';
            this.addChild(this.effectLayer);

            this.transitionLayer = new cssa.Layer();
            this.transitionLayer.el.className += ' transition-layer';
            this.addChild(this.transitionLayer);

            this.casts = {};

            this.currentScripts = [];
            this.textIndex = 0;
            this.sceneIndex = 0;
        }
        Drama.prototype.setup = function (settings) {
            this.settings = settings;
            this.setSize(settings.screenSize);
            this.scene.setSize(settings.screenSize);
            this.scene.bg.setSize(settings.screenSize);
            this.scene.characterLayer.setSize(settings.screenSize);
            this.captionLayer.setSize(settings.screenSize);
            this.effectLayer.setSize(settings.screenSize);
            this.transitionLayer.setSize(settings.screenSize);
            this.caption.setSize(settings.captionSize);
            this.caption.setPosition(settings.captionPosition);

            this.scene.setup(settings.scenes);
            this.changeScene(0);

            return this;
        };
        Drama.prototype.next = function () {
            if ($.when.apply(this, this.currentDeferreds).state() == 'resolved') {
                if (!this.nextText()) {
                    return this.nextScene();
                }
                return true;
            }
            ;
            return true;
        };
        Drama.prototype.nextText = function () {
            if (this.textIndex < (this.currentScripts.length - 1)) {
                this.textIndex++;
                var txt = this.currentScripts[this.textIndex];
                this.caption.setText(txt);
                this.currentDeferreds.push(cssa.waitFor(this.wait));
                return true;
            } else {
                return false;
            }
        };
        Drama.prototype.prevText = function () {
            return this;
        };
        Drama.prototype.changeScene = function (sceneIndex) {
            this.sceneIndex = sceneIndex;
            this.currentDeferreds = [];
            this.textIndex = 0;
            var currentScene = this.settings.scenes[this.sceneIndex];
            this.currentScripts = currentScene.scripts;
            var txt = this.currentScripts[this.textIndex];
            this.caption.setTitle(currentScene.title);
            this.caption.setText(this.currentScripts[this.textIndex]);
            this.wait = 100;
            this.currentDeferreds.push(cssa.waitFor(this.wait));

            if (currentScene.transition) {
                var def = this.transitionLayer.transition(currentScene.transition, 'webkitAnimationEnd');
                this.currentDeferreds.push(def);
            }

            if (currentScene.effect) {
                var effect = new cssa.Effect();
                effect.setup(currentScene.effect);
                var pos = {
                    x: this.size.width / 2,
                    y: this.size.height / 2
                };
                effect.setPosition(pos);
                this.effectLayer.addChild(effect);
                effect.invoke();
            }

            this.scene.change(currentScene);

            Array.prototype.push.apply(this.currentDeferreds, this.scene.currentDeferreds);
            return this;
        };
        Drama.prototype.nextScene = function () {
            if (this.sceneIndex >= this.settings.scenes.length - 1) {
                return false;
            } else {
                this.sceneIndex++;
                this.changeScene(this.sceneIndex);
                return true;
            }
        };
        Drama.prototype.prevScene = function () {
            this.sceneIndex--;
            this.textIndex = 0;
            return this;
        };
        return Drama;
    })(cssa.Layer);
    cssa.Drama = Drama;
})(cssa || (cssa = {}));
