/// <reference path="cssa.sprite.ts" />
module cssa{
	export class SpriteAnimation extends cssa.Sprite{
		settings;
		constructor(){
			super();
			this.el.className += ' sprite-animation';
		}
		setup(name:string){
			var asset = document.querySelector('.assets .asset[data-name="'+name+'"]');
			this.setAsset(asset);
			var bd;// base dimension
			var w = this.size.width;
			var h = this.size.height;
			var cptl = getComputedStyle(asset);
			var settings:any = {};
			settings.duration = Math.round(parseFloat(cptl.webkitAnimationDuration)*1000);
			settings.delay = Math.round(parseFloat(cptl.webkitAnimationDelay)*1000);
			settings.iterationCount = cptl.webkitAnimationIterationCount;
			var sprite_size = cptl.backgroundSize.split(" ");
			var sprite_width = parseInt(sprite_size[0]);
			var sprite_height = parseInt(sprite_size[1]);
			// 縦方向か横方向か決める
			var is_horizontal = Boolean(sprite_width>w);
			if(is_horizontal){
				settings.frames = ~~(sprite_width/w);
				bd = w;
			} else {
				settings.frames = ~~(sprite_height/h);
				bd = h;
			}
			this.settings = settings;
			settings.id = asset.getAttribute("data-id");
			if (settings.id === null){
				settings.id = "sprite-animation-"+Date.now();
			}
			this.el.id = settings.id;
			if(!document.getElementById("style-"+name)){
				var styleText = "";
				styleText += "[data-name='"+name+"'].invoke {";
				styleText += "-webkit-animation-name:kf-"+name+";";
				styleText += "}";
				var tick = 0.0001;
				var style = document.createElement("style");
				style.id = "style-"+name;
				var imax = settings.frames;
				var i = 0;
				var frame = 0;
				styleText += "@-webkit-keyframes kf-"+name+" {";
				for(i;i<imax;i++){
					var fromPercent = (i == 0)?"from":(i/imax*100).toFixed(4)+"%";
					var toPercent = (i == (imax-1))?"to":((i+1)/imax*100-tick).toFixed(4)+"%";
					var dist = frame * bd * -1;
					styleText += fromPercent+","+toPercent+" {background-position:";
					styleText += (is_horizontal)?dist +"px 0}":styleText += "0 "+ dist +"px}";
					frame++;
				}
				styleText += "}";
				style.appendChild(document.createTextNode(styleText));
				document.getElementsByTagName('head')[0].appendChild(style);
			}
			return this;
		}
		invoke(){
			var selector = "#" + this.el.id;
			var dfd = $.Deferred();
			var count = 0;
			var visibility = this.visible;
			if(this.settings.iterationCount != 'infinite'){
				var onEndFunc = ()=>{
					this.$asset.removeClass("invoke");
					this.visible = visibility;
					cssa.nextTick(dfd.resolve);
				}
				cssa.$doc.one('webkitAnimationEnd',selector,onEndFunc);
			}
			this.show();
			this.$asset.addClass("invoke");
			return dfd.promise();
		}
		revoke(){
			this.$asset.removeClass("invoke");
		}
	}
}