/// <reference path="cssa.unit.ts" />
module cssa{
	export class Caption extends cssa.Sprite{
		title:string;
		text:string;
		titleHolder:HTMLElement;
		textHolder:HTMLElement;
		constructor(){
			super();
			this.el.className += ' caption';
			var div = document.createElement('div');
			div.className = 'title-plate';
			var p = document.createElement('p');
			this.el.appendChild(div);
			this.el.appendChild(p);
			this.titleHolder = div;
			this.textHolder = p;
		}
		setTitle(title){
			this.title = title;
			this.titleHolder.innerHTML = title;
		}
		setText(text){
			this.text = text;
			this.textHolder.innerHTML = text;
		}
	}
}