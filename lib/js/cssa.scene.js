var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Scene = (function (_super) {
        __extends(Scene, _super);
        function Scene() {
            _super.call(this);
            this.el.className += ' scene';

            this.bg = new cssa.Layer();
            this.bg.el.className += ' bg-layer';
            this.addChild(this.bg);

            this.characterLayer = new cssa.Layer();
            this.characterLayer.el.className += ' character-layer';
            this.addChild(this.characterLayer);

            this.casts = [];
            this.currentDeferreds = [];
        }
        Scene.prototype.setup = function (scenes) {
            var imax = scenes.length;
            for (var i = 0; i < imax; i++) {
                var jmax = scenes[i].characters.length;
                for (var j = 0; j < jmax; j++) {
                    var actor = new cssa.Actor();
                    actor.setup(scenes[i].characters[j].id);
                    this.addCast(actor);
                }
            }
        };
        Scene.prototype.addCast = function (cast) {
            if (!(cast instanceof cssa.Actor)) {
                throw new Error('castはcssa.Actorでなければならない');
            }
            var id = cast.actor_id;

            if (!this.casts[id]) {
                this.casts[id] = cast;
                this.characterLayer.addChild(cast);
            }
            return this;
        };
        Scene.prototype.change = function (sceneObject) {
            this.currentDeferreds = [];

            if (this.bg.el.getAttribute('data-bg') != sceneObject.bg) {
                this.bg.el.setAttribute('data-bg', sceneObject.bg ? sceneObject.bg : '');
            }

            if (sceneObject.action) {
                this.action(sceneObject.action);
            } else {
                this.resetAction();
            }

            var jmax = sceneObject.characters.length;
            var currentCasts = [];
            for (var j = 0; j < jmax; j++) {
                var charaSetting = sceneObject.characters[j];
                var id = charaSetting.id;
                var pos = charaSetting.position;
                var cast = this.casts[id];
                currentCasts.push(cast);
                if (charaSetting.isActive) {
                    cast.setPosition(pos).activate().show();
                } else {
                    cast.setPosition(pos).deactivate().show();
                }

                if (charaSetting.transition) {
                    var def = cast.transition(charaSetting.transition);
                    this.currentDeferreds.push(def);
                }

                if (charaSetting.action) {
                    cast.action(charaSetting.action);
                } else {
                    cast.resetAction();
                }
            }

            for (var i in this.casts) {
                if (currentCasts.indexOf(this.casts[i]) == -1 && this.casts[i].visible) {
                    this.casts[i].hide();
                }
            }
            currentCasts = null;
            return this;
        };
        return Scene;
    })(cssa.Layer);
    cssa.Scene = Scene;
})(cssa || (cssa = {}));
