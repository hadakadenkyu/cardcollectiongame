var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Unit = (function (_super) {
        __extends(Unit, _super);
        function Unit() {
            _super.call(this);
            this.el.className += ' unit';
        }
        Unit.prototype.setup = function (name) {
            var orig_asset = document.querySelector('.assets .asset[data-name="' + name + '"]');

            this.hitPoint = new cssa.Status();
            var hp = orig_asset.getAttribute("data-hit-point") ? Number(orig_asset.getAttribute("data-hit-point")) : 100;
            var maxHp = orig_asset.getAttribute("data-max-hit-point") ? Number(orig_asset.getAttribute("data-max-hit-point")) : 100;
            var low = orig_asset.getAttribute("data-low") ? Number(orig_asset.getAttribute("data-low")) : 0.1;
            this.hitPoint.setup({ value: hp, maxValue: maxHp, low: low, name: "HP" });
            this.src = orig_asset.getAttribute("data-src") ? orig_asset.getAttribute("data-src") : "";
            var img = new Image();
            var cstl = getComputedStyle(orig_asset);
            img.width = parseInt(cstl.width);
            img.height = parseInt(cstl.height);
            img.src = this.src;

            var daef = document.createElement("div");
            daef.className = 'damage-effect';
            var mskd = document.createElement("div");
            mskd.style.webkitMaskImage = 'url("' + this.src + '")';
            mskd.style.webkitMaskSize = 'contain';
            daef.appendChild(mskd);

            var deef = daef.cloneNode(true);
            deef.className = 'defeat-effect';
            var puef = daef.cloneNode(true);
            puef.className = 'purify-effect';

            this.$damageEffect = $(daef);
            this.$defeatEffect = $(deef);
            this.$purifyEffect = $(puef);
            this.setAsset(orig_asset);

            this.$el.find('.asset').append(img, daef, deef, puef);
            return this;
        };
        Unit.prototype.setupCut = function () {
            var w = this.size.width;
            var h = this.size.height;
            var src = this.$el.find("img").attr('src');
            var $el = $("<div class='body-parts'><div class='upper-body'></div><div class='lower-body'></div></div>");
            $el.find(".upper-body,.lower-body").css({ width: w + "px", height: h + "px", backgroundImage: "url(" + src + ")" });
            this.$el.children(".asset").append($el);
            return this;
        };
        Unit.prototype.cut = function () {
            var dfd = $.Deferred();
            var bdy = this.$el.find(".body-parts");

            var cnt = 0;
            var onEndFunc = function (event) {
                cnt++;
                if (cnt >= 3) {
                    cssa.$doc.off("webkitTransitionEnd", selector, onEndFunc);
                    dfd.resolve();
                }
                return false;
            };

            this.$el.find("img").hide();
            var selector = "#" + this.el.id + " .body-parts";
            cssa.$doc.on("webkitTransitionEnd", selector, onEndFunc);
            bdy.addClass("invoke");
            return dfd.promise();
        };
        Unit.prototype.flash = function (duration, callback) {
        };
        Unit.prototype.damage = function (damagePoint) {
            var _this = this;
            var dfd = $.Deferred();
            var dp = (damagePoint === undefined) ? 0 : damagePoint * -1;

            var onEndFunc = function (event) {
                _this.$damageEffect.removeClass("invoke");
                dfd.resolve();
                return false;
            };
            this.$damageEffect.removeClass("invoke");
            var selector = "#" + this.el.id + " .damage-effect";
            cssa.$doc.one("webkitTransitionEnd", selector, onEndFunc);
            this.$damageEffect.addClass("invoke");
            this.hitPoint.addValue(dp);
            return dfd.promise();
        };
        Unit.prototype.defeat = function () {
            var _this = this;
            var dfd = $.Deferred();

            var onEndFunc = function (event) {
                _this.$defeatEffect.hide();
                dfd.resolve();
                return false;
            };

            this.$el.find("img").hide();
            var selector = "#" + this.el.id + " .defeat-effect";
            cssa.$doc.one("webkitTransitionEnd", selector, onEndFunc);
            this.$defeatEffect.addClass("invoke");
            return dfd.promise();
        };
        Unit.prototype.purify = function () {
            var _this = this;
            var dfd = $.Deferred();

            var onEndFunc = function (event) {
                _this.$purifyEffect.hide().removeClass("invoke");
                dfd.resolve();
                return false;
            };

            this.$el.find(".monster-image").hide();
            var selector = "#" + this.el.id + " .purify-effect";
            cssa.$doc.one("webkitTransitionEnd", selector, onEndFunc);

            this.$purifyEffect.addClass("invoke");
            return dfd.promise();
        };
        Unit.prototype.charge = function (targetPos, func) {
            var _this = this;
            var origPos = { x: this.position.x, y: this.position.y };
            var func = func ? func : Function;
            return this.moveTo(targetPos, { webkitTransitionDuration: '250ms' }).then(function () {
                func();
                return _this.moveTo(origPos, { webkitTransitionDuration: '250ms' });
            });
        };
        return Unit;
    })(cssa.Sprite);
    cssa.Unit = Unit;
})(cssa || (cssa = {}));
