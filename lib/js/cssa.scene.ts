/// <reference path="cssa.layer.ts" />
module cssa{
	export class Scene extends cssa.Layer{
		bg;
		characterLayer;
		casts;
		currentDeferreds;
		constructor(){
			super();
			this.el.className += ' scene';

			this.bg = new cssa.Layer();
			this.bg.el.className += ' bg-layer';
			this.addChild(this.bg);

			this.characterLayer = new cssa.Layer();
			this.characterLayer.el.className += ' character-layer';
			this.addChild(this.characterLayer);

			this.casts = [];
			this.currentDeferreds = [];
		}
		setup(scenes){
			// 全キャストを先に載せる
			var imax = scenes.length;
			for(var i=0;i<imax;i++){
				var jmax = scenes[i].characters.length;
				for (var j=0;j<jmax;j++){
					var actor = new cssa.Actor();
					actor.setup(scenes[i].characters[j].id);
					this.addCast(actor);
				}
			}
		}
		addCast(cast){
			if(!(cast instanceof cssa.Actor)){
				throw new Error('castはcssa.Actorでなければならない');
			}
			var id = cast.actor_id;
			// 重複防止
			if (!this.casts[id]){
				this.casts[id] = cast;
				this.characterLayer.addChild(cast);
			}
			return this;
		}
		change(sceneObject){
			this.currentDeferreds = [];
			// シーン背景
			if(this.bg.el.getAttribute('data-bg') != sceneObject.bg){
				this.bg.el.setAttribute('data-bg',sceneObject.bg?sceneObject.bg:'');
			}

			// シーンアクション
			if(sceneObject.action){
				this.action(sceneObject.action);
			} else {
				this.resetAction();
			}

			// キャスト
			var jmax = sceneObject.characters.length;
			var currentCasts = [];
			for (var j=0;j<jmax;j++){
				var charaSetting = sceneObject.characters[j];
				var id = charaSetting.id;
				var pos = charaSetting.position;
				var cast = this.casts[id];
				currentCasts.push(cast);
				if(charaSetting.isActive){
					cast.setPosition(pos).activate().show();
				} else {
					cast.setPosition(pos).deactivate().show();
				}
				// キャストトランジション
				if(charaSetting.transition){
					var def = cast.transition(charaSetting.transition);
					this.currentDeferreds.push(def);
				}
				// キャストアクション
				if(charaSetting.action){
					cast.action(charaSetting.action);
				} else {
					cast.resetAction();
				}
			}
			// 必要ないものは消す
			for (var i in this.casts){
				if(currentCasts.indexOf(this.casts[i]) == -1 && this.casts[i].visible){
					this.casts[i].hide();
				}
			}
			currentCasts = null;
			return this;
		}
	}
}