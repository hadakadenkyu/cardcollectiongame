var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Status = (function (_super) {
        __extends(Status, _super);
        function Status() {
            _super.call(this);
            this.el.className += ' status';
            this.minValue = 0;
            this.low = 0.1;
        }
        Status.prototype.setup = function (obj) {
            this.maxValue = obj.maxValue;
            this.minValue = (typeof (obj.minValue) == 'number') ? obj.minValue : 0;
            this.low = (typeof (obj.low) == 'number') ? obj.low : 0.1;
            this.setAsset(Status.asset);
            this.$el.find(".max-value").text(this.maxValue);
            this.setValue(obj.value);
        };
        Status.prototype.setValue = function (val) {
            var val = Math.max(val, this.minValue);
            val = Math.min(val, this.maxValue);
            this.value = val;

            var w = Number((val / this.maxValue).toFixed(2));
            var isLow = Boolean(this.low >= w);
            var prop = {};
            if (cssa.isIOS) {
                prop.webkitTransform = cssa.getTransformProperty("scale", w + ",1");
            } else {
                prop.width = (w * 100) + "%";
            }
            this.$el.toggleClass("low", isLow).find(".current-value").text(val);
            this.$el.find(".bar").css(prop);
            return val;
        };
        Status.prototype.addValue = function (val) {
            return this.setValue(this.value + val);
        };
        Status.asset = (function () {
            var asset = document.createElement('div');
            asset.className = 'asset';
            asset.setAttribute('data-role', 'status');
            asset.innerHTML = '<div class="bar-holder"><div class="bar"></div></div><div class="value"><span class="current-value">0</span>/<span class="max-value">0</span>';
            return asset;
        })();
        return Status;
    })(cssa.Sprite);
    cssa.Status = Status;
})(cssa || (cssa = {}));
