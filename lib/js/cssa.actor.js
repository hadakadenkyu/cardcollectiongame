var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var cssa;
(function (cssa) {
    var Actor = (function (_super) {
        __extends(Actor, _super);
        function Actor() {
            _super.call(this);
            this.el.className += ' actor';
            this.actor_id = '';
            this.visible = false;
        }
        Actor.prototype.setup = function (name) {
            var asset = document.querySelector('.assets .asset[data-name="' + name + '"]');
            this.setAsset(asset);
            this.actor_id = name;
            this.$el.removeAttr('data-transition').removeAttr('data-action');
            return this;
        };

        Actor.prototype.activate = function () {
            this.visible = true;
            this.$el.addClass('active');
            return this;
        };
        Actor.prototype.deactivate = function () {
            this.$el.removeClass('active');
            return this;
        };
        return Actor;
    })(cssa.Sprite);
    cssa.Actor = Actor;
})(cssa || (cssa = {}));
