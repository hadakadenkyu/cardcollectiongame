/// <reference path="../ccg.ts" />
/// <reference path="../model/cardModel.ts" />
"use strict";
// カードモデル
module CCG{// カードのCollection
	export class CardCollection extends Backbone.Collection{
		localStorage;
	    model = CCG.CardModel;
	    constructor(options={}){
	    	super(options);
	    }
	}
}