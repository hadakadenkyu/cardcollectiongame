"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var CardCollection = (function (_super) {
        __extends(CardCollection, _super);
        function CardCollection(options) {
            if (typeof options === "undefined") { options = {}; }
            _super.call(this, options);
            this.model = CCG.CardModel;
        }
        return CardCollection;
    })(Backbone.Collection);
    CCG.CardCollection = CardCollection;
})(CCG || (CCG = {}));
//@ sourceMappingURL=CardCollection.js.map
