"use strict";
var CCG;
(function (CCG) {
    CCG.unitDataMaster;
    CCG.weaponDataMaster;
    var history_go = window.history.go;
    var history_back = window.history.back;
    CCG.settings = {};
    function disableHistoryBack() {
        window.history.go = function () {
        };
        window.history.back = function () {
        };
    }
    CCG.disableHistoryBack = disableHistoryBack;
    function enableHistoryBack() {
        window.history.go = history_go;
        window.history.back = history_back;
    }
    CCG.enableHistoryBack = enableHistoryBack;
    function capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    CCG.capitaliseFirstLetter = capitaliseFirstLetter;
    function toCamelCase(string) {
        return string.replace(/(\-([a-z]))/g, function ($1, $2, $3) {
            return $3.toUpperCase();
        });
    }
    CCG.toCamelCase = toCamelCase;
    CCG.appView;
    function initialize($appElem) {
        var jqmini = $appElem.jqMini({
            transition: "none",
            scrollCheck: false,
            external: "external",
            hash: false
        });
        var uuid;
        if (typeof (window.localStorage) !== 'object') {
            $('body').empty().html('local storage is required');
        } else {
            if (!(uuid = localStorage.getItem('uuid'))) {
                uuid = UUID.generate();
                localStorage.setItem('uuid', uuid);
            }
            CCG.appView = new CCG.AppView({ el: $appElem.get(0), jqmini: jqmini, uuid: uuid });
        }
    }
    CCG.initialize = initialize;
})(CCG || (CCG = {}));
//@ sourceMappingURL=ccg.js.map
