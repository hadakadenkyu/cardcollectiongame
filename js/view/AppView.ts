/// <reference path="../ccg.ts" />
/// <reference path="../model/CardModel.ts" />
/// <reference path="../model/FooterModel.ts" />
/// <reference path="../model/PlayerStatusModel.ts" />
/// <reference path="../collection/CardCollection.ts" />
/// <reference path="./FooterView.ts" />
/// <reference path="./PlayerStatusView.ts" />
"use strict";
// カードモデル
module CCG{// カードのCollection
	export class AppView extends Backbone.View{
        jqmini:any;
        footerView:CCG.FooterView;
        playerStatusView:CCG.PlayerStatusView;
        playerCardCollection:CCG.CardCollection;
        troop1CardCollection:CCG.CardCollection;
        troop2CardCollection:CCG.CardCollection;
        troop3CardCollection:CCG.CardCollection;
        troop4CardCollection:CCG.CardCollection;
        uuid:string;
		constructor(options){
			super(options);
			this.jqmini = options.jqmini;
			this.uuid = options.uuid;
			var self = this;
			/* 各ページで共通のView,modelを作る */
			/* サーバ側が出来たらこの辺は初回サーバ問い合わせを行なってdoneでsetupする */
			var footerModel = new CCG.FooterModel;
			this.footerView = new CCG.FooterView({el:this.el.querySelector('.footer-contents'),template:this.el.querySelector("#template-footer").innerHTML,model:footerModel});
			var playerStatusModel = new CCG.PlayerStatusModel({id:this.uuid});
			playerStatusModel.fetch();
			this.playerStatusView = new CCG.PlayerStatusView({el:this.el.querySelector('.player-status'),template:this.el.querySelector("#template-player-status").innerHTML,model:playerStatusModel});
			this.playerCardCollection = new CCG.CardCollection;
			this.playerCardCollection.localStorage = new Backbone.LocalStorage("PlayerCardCollection");
			this.playerCardCollection.fetch();
			this.troop1CardCollection = new CCG.CardCollection;
			this.troop1CardCollection.localStorage = new Backbone.LocalStorage("Troop1CardCollection");
			this.troop1CardCollection.fetch();
			this.troop2CardCollection = new CCG.CardCollection;
			this.troop2CardCollection.localStorage = new Backbone.LocalStorage("Troop2CardCollection");
			this.troop2CardCollection.fetch();
			this.troop3CardCollection = new CCG.CardCollection;
			this.troop3CardCollection.localStorage = new Backbone.LocalStorage("Troop3CardCollection");
			this.troop3CardCollection.fetch();
			this.troop4CardCollection = new CCG.CardCollection;
			this.troop4CardCollection.localStorage = new Backbone.LocalStorage("Troop4CardCollection");
			this.troop4CardCollection.fetch();
			// thisの固定
			_.bindAll(this,'updateGeneralStatus','goTo','toggleHover');
			/* pageがviewに対応するのでViewをひたすら生成 */
			this.$el.children(".page").each((o,el)=>{
				var id = el.getAttribute("id");
				var options:any = {};
				options.el = el;
				options.app = this;
				if(id == "player-card-list-page"){
					options.collection = this.playerCardCollection;
				}
				var varName = CCG.toCamelCase(id+"-view");
				var modelName = CCG.capitaliseFirstLetter(varName);
				el[varName] = new CCG[modelName](options);
                if(o==0){
                    $(el).trigger('preshow')
                }
			})
			this.events = {
				'click a[data-href-id]':'goTo',
				'touchstart a,button,input,select,.button':'toggleHover',
				'touchend a,button,input,select,.button':'toggleHover'
			}
			this.delegateEvents();
			// 初期データ通信
			var dfd = $.Deferred();
			dfd.then(()=>{
				return $.ajax({
					url:'./weaponDataMaster.json',
					dataType: "json"
				}).done(function(res){
					CCG.weaponDataMaster = res;
				}).fail(function(){
				}).always(function(){
				});
			}).then(()=>{
				return $.ajax({
					url:'./unitDataMaster.json',
					dataType: "json"
				}).done((res)=>{
					CCG.unitDataMaster = res;
				}).fail(function(){
				}).always(function(){
				});
			}).then(()=>{
				// カードが一枚も存在しない場合（つまり初回プレイ時）
				if(this.playerCardCollection.length == 0){
					var model1 = new CCG.CardModel(CCG.unitDataMaster['warrior-0001']);
					this.playerCardCollection.create([model1]);
					this.troop1CardCollection.create([model1]);
				}
				this.$el.removeClass('loading');
			});
			dfd.resolve();
		}
		toggleHover(event){
			$(event.currentTarget).toggleClass("hover",event.type == "touchstart");
		}
		goTo(event){
			var self = event.currentTarget;

			var selector = self.getAttribute("data-href-id");
			var props,prop,i,imax,dataObj;
			props = self.getAttribute.split("&");
			var page = this.el.querySelector(selector);
			// コンテナがあったらヘッダー移動
			page.querySelector('.player-status-container').appendChild(this.playerStatusView.el);
			// コンテナがあったらフッター移動
			page.querySelector('footer').appendChild(this.footerView.el);
			if(props){
				imax = props.length;
				dataObj = {};
				for(i in props){
					prop = props[i].split("=");
					// dataObj["data-"+prop[0]] = prop[1];
					page.setAttribute("data-"+prop[0],prop[1]);
				}
			}
			this.jqmini.goTo(selector);
			return false;
		}
		updateGeneralStatus(data){
			// player status更新
			if(typeof(data.player) == 'object' && typeof(data.player.status) == 'object'){
				var response = this.playerStatusView.model.parse(data.player.status);
				this.playerStatusView.model.set(response);
			}
		}
	}
}