"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var FooterView = (function (_super) {
        __extends(FooterView, _super);
        function FooterView() {
            _super.apply(this, arguments);
        }
        FooterView.prototype.initialize = function (options) {
            _.bindAll(this, 'render');
            this.template = options.template;
            this.model.bind("change", this.render);
            this.render();
            return this;
        };
        FooterView.prototype.render = function () {
            this.el.innerHTML = _.template(this.template, {});

            return this;
        };
        return FooterView;
    })(Backbone.View);
    CCG.FooterView = FooterView;
})(CCG || (CCG = {}));
//@ sourceMappingURL=FooterView.js.map
