/// <reference path="./BasePageView.ts" />
"use strict";
// カードモデル
module CCG{// カードのCollection
    export class PlayerStatusView extends Backbone.View{
    	template:string;
        initialize(options) {
        	this.template = options.template;
            _.bindAll(this, "render");  // render実行時のthisをインスタンスに固定
            this.model.bind("change", this.render); // modelが変更された時render
            this.render(); // 初回実行
        }
        render(){
        	var htmlText = _.template(this.template,this.model.attributes);
        	this.el.innerHTML = htmlText;
            return this;
        }
    }
}