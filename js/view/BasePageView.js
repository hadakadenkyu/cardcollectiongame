"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var BasePageView = (function (_super) {
        __extends(BasePageView, _super);
        function BasePageView() {
            _super.apply(this, arguments);
        }
        BasePageView.prototype.initialize = function (options) {
            var self = this;
            this.app = options.app;
            this.el = (typeof (options.el) !== 'undefined') ? options.el : document.createElement('div');
            this.footerContainer = this.el.querySelector("footer");
            this.playerStatusContainer = this.el.querySelector(".player-status-container");
            _.bindAll(this, '_onShow', '_onPreShow');
            this.events = (typeof this.events == 'object') ? this.events : {};
            this.events = $.extend({
                "show": "_onShow",
                "preshow": "_onPreShow"
            }, this.events);
            this.delegateEvents();
        };
        BasePageView.prototype._onShow = function () {
        };
        BasePageView.prototype._onPreShow = function () {
            this.playerStatusContainer.appendChild(this.app.playerStatusView.el);
            this.app.footerView.$el.find("a").removeClass("selected").filter("[href='#" + this.el.id + "']").addClass("selected");
            this.footerContainer.appendChild(this.app.footerView.el);
        };
        return BasePageView;
    })(Backbone.View);
    CCG.BasePageView = BasePageView;
})(CCG || (CCG = {}));
//@ sourceMappingURL=BasePageView.js.map
