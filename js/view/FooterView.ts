/// <reference path="../ccg.ts" />
"use strict";
// カードモデル
module CCG{// カードのCollection
    export class FooterView extends Backbone.View{// フッターのビュー
        template:string;
        initialize(options){
            _.bindAll(this, 'render');
            this.template = options.template;
            this.model.bind("change",this.render);
            this.render();
            return this;
        }
        render () {
            this.el.innerHTML = _.template(this.template,{});
            //var cnt = this.model.get("count");
            //this.$badge.text(cnt).toggle(Boolean(cnt));
            return this;
        }
    }
}