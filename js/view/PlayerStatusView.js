"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var PlayerStatusView = (function (_super) {
        __extends(PlayerStatusView, _super);
        function PlayerStatusView() {
            _super.apply(this, arguments);
        }
        PlayerStatusView.prototype.initialize = function (options) {
            this.template = options.template;
            _.bindAll(this, "render");
            this.model.bind("change", this.render);
            this.render();
        };
        PlayerStatusView.prototype.render = function () {
            var htmlText = _.template(this.template, this.model.attributes);
            this.el.innerHTML = htmlText;
            return this;
        };
        return PlayerStatusView;
    })(Backbone.View);
    CCG.PlayerStatusView = PlayerStatusView;
})(CCG || (CCG = {}));
//@ sourceMappingURL=PlayerStatusView.js.map
