/// <reference path="../ccg.ts" />
"use strict";
// カードモデル
module CCG{
    export class BasePageView extends Backbone.View {
        app:any;
        footerContainer;
        playerStatusContainer;
        initialize(options){
            var self = this;
            this.app = options.app;
            this.el = (typeof(options.el)!=='undefined')?options.el:document.createElement('div');
            this.footerContainer = this.el.querySelector("footer");
            this.playerStatusContainer = this.el.querySelector(".player-status-container");
            _.bindAll(this, '_onShow','_onPreShow');
            this.events = (typeof this.events == 'object')?this.events:{};
            this.events=$.extend({
                "show":"_onShow",
                "preshow":"_onPreShow"
            },this.events);
            this.delegateEvents();
        }
        _onShow(){
        }
        _onPreShow(){
            this.playerStatusContainer.appendChild(this.app.playerStatusView.el);
            this.app.footerView.$el.find("a").removeClass("selected").filter("[href='#"+this.el.id+"']").addClass("selected");
            this.footerContainer.appendChild(this.app.footerView.el);
        }
    }
}