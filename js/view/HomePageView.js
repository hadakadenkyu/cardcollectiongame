"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var HomePageView = (function (_super) {
        __extends(HomePageView, _super);
        function HomePageView() {
            _super.apply(this, arguments);
        }
        return HomePageView;
    })(CCG.BasePageView);
    CCG.HomePageView = HomePageView;
})(CCG || (CCG = {}));
