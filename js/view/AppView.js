"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var AppView = (function (_super) {
        __extends(AppView, _super);
        function AppView(options) {
            var _this = this;
            _super.call(this, options);
            this.jqmini = options.jqmini;
            this.uuid = options.uuid;
            var self = this;

            var footerModel = new CCG.FooterModel();
            this.footerView = new CCG.FooterView({ el: this.el.querySelector('.footer-contents'), template: this.el.querySelector("#template-footer").innerHTML, model: footerModel });
            var playerStatusModel = new CCG.PlayerStatusModel({ id: this.uuid });
            playerStatusModel.fetch();
            this.playerStatusView = new CCG.PlayerStatusView({ el: this.el.querySelector('.player-status'), template: this.el.querySelector("#template-player-status").innerHTML, model: playerStatusModel });
            this.playerCardCollection = new CCG.CardCollection();
            this.playerCardCollection.localStorage = new Backbone.LocalStorage("PlayerCardCollection");
            this.playerCardCollection.fetch();
            this.troop1CardCollection = new CCG.CardCollection();
            this.troop1CardCollection.localStorage = new Backbone.LocalStorage("Troop1CardCollection");
            this.troop1CardCollection.fetch();
            this.troop2CardCollection = new CCG.CardCollection();
            this.troop2CardCollection.localStorage = new Backbone.LocalStorage("Troop2CardCollection");
            this.troop2CardCollection.fetch();
            this.troop3CardCollection = new CCG.CardCollection();
            this.troop3CardCollection.localStorage = new Backbone.LocalStorage("Troop3CardCollection");
            this.troop3CardCollection.fetch();
            this.troop4CardCollection = new CCG.CardCollection();
            this.troop4CardCollection.localStorage = new Backbone.LocalStorage("Troop4CardCollection");
            this.troop4CardCollection.fetch();

            _.bindAll(this, 'updateGeneralStatus', 'goTo', 'toggleHover');

            this.$el.children(".page").each(function (o, el) {
                var id = el.getAttribute("id");
                var options = {};
                options.el = el;
                options.app = _this;
                if (id == "player-card-list-page") {
                    options.collection = _this.playerCardCollection;
                }
                var varName = CCG.toCamelCase(id + "-view");
                var modelName = CCG.capitaliseFirstLetter(varName);
                el[varName] = new CCG[modelName](options);
                if (o == 0) {
                    $(el).trigger('preshow');
                }
            });
            this.events = {
                'click a[data-href-id]': 'goTo',
                'touchstart a,button,input,select,.button': 'toggleHover',
                'touchend a,button,input,select,.button': 'toggleHover'
            };
            this.delegateEvents();

            var dfd = $.Deferred();
            dfd.then(function () {
                return $.ajax({
                    url: './weaponDataMaster.json',
                    dataType: "json"
                }).done(function (res) {
                    CCG.weaponDataMaster = res;
                }).fail(function () {
                }).always(function () {
                });
            }).then(function () {
                return $.ajax({
                    url: './unitDataMaster.json',
                    dataType: "json"
                }).done(function (res) {
                    CCG.unitDataMaster = res;
                }).fail(function () {
                }).always(function () {
                });
            }).then(function () {
                if (_this.playerCardCollection.length == 0) {
                    var model1 = new CCG.CardModel(CCG.unitDataMaster['warrior-0001']);
                    _this.playerCardCollection.create([model1]);
                    _this.troop1CardCollection.create([model1]);
                }
                _this.$el.removeClass('loading');
            });
            dfd.resolve();
        }
        AppView.prototype.toggleHover = function (event) {
            $(event.currentTarget).toggleClass("hover", event.type == "touchstart");
        };
        AppView.prototype.goTo = function (event) {
            var self = event.currentTarget;

            var selector = self.getAttribute("data-href-id");
            var props, prop, i, imax, dataObj;
            props = self.getAttribute.split("&");
            var page = this.el.querySelector(selector);

            page.querySelector('.player-status-container').appendChild(this.playerStatusView.el);

            page.querySelector('footer').appendChild(this.footerView.el);
            if (props) {
                imax = props.length;
                dataObj = {};
                for (i in props) {
                    prop = props[i].split("=");

                    page.setAttribute("data-" + prop[0], prop[1]);
                }
            }
            this.jqmini.goTo(selector);
            return false;
        };
        AppView.prototype.updateGeneralStatus = function (data) {
            if (typeof (data.player) == 'object' && typeof (data.player.status) == 'object') {
                var response = this.playerStatusView.model.parse(data.player.status);
                this.playerStatusView.model.set(response);
            }
        };
        return AppView;
    })(Backbone.View);
    CCG.AppView = AppView;
})(CCG || (CCG = {}));
//@ sourceMappingURL=AppView.js.map
