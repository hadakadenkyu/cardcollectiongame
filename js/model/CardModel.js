"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var CardModel = (function (_super) {
        __extends(CardModel, _super);
        function CardModel() {
            _super.apply(this, arguments);
        }
        CardModel.prototype.destroy = function () {
            this.trigger("destroy", this);
        };

        CardModel.prototype.validate = function (attrs) {
            if (_.isString(attrs.name) && _.isEmpty(attrs.name)) {
                return "task name is empty.";
            }
        };
        return CardModel;
    })(Backbone.Model);
    CCG.CardModel = CardModel;
})(CCG || (CCG = {}));
//@ sourceMappingURL=CardModel.js.map
