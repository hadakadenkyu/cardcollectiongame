/// <reference path="../ccg.ts" />
"use strict";
module CCG{
    export class PlayerStatusModel extends Backbone.Model {
        defaults():playerStatus{
            return {
                level:1,
                exp:0,
                resource1:100,
                resource2:100,
                resource3:100,
                resource4:100,
                facility1:2,
                facility2:2,
                party:1
            }
        }
        localStorage:any;
        initialize(){
            this.localStorage = new Backbone.LocalStorage("PlayerStatus") // Unique name within your app.;
        }
        parse(response:any):any {
            return response;
        }
    }
}