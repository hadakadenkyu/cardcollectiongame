/// <reference path="../ccg.ts" />
"use strict";
// カードモデル
module CCG{
	export class CardModel extends Backbone.Model {
	    // destroy をオーバーライド。
	    // 本来ならサーバーと通信するけど、今回のサンプルではデータを永続化しないから、
	    // destroy イベントだけ発生させる。
	    destroy() {
	        this.trigger("destroy", this);
	    }

	    // set メソッドに渡されたデータを検証する。
	    // 何か値を返すと検証エラー扱いになるので、
	    // 不正な値だったときはエラーメッセージなんかを返すといい。
	    validate(attrs: any) : string {
	        // 検証には、underscore の便利メソッドを使っている。
	        if (_.isString(attrs.name) && _.isEmpty(attrs.name)) {
	            return "task name is empty.";
	        }
	    }
	}
}