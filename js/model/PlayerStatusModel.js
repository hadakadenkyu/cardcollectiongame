"use strict";
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CCG;
(function (CCG) {
    var PlayerStatusModel = (function (_super) {
        __extends(PlayerStatusModel, _super);
        function PlayerStatusModel() {
            _super.apply(this, arguments);
        }
        PlayerStatusModel.prototype.defaults = function () {
            return {
                level: 1,
                exp: 0,
                resource1: 100,
                resource2: 100,
                resource3: 100,
                resource4: 100,
                facility1: 2,
                facility2: 2,
                party: 1
            };
        };

        PlayerStatusModel.prototype.initialize = function () {
            this.localStorage = new Backbone.LocalStorage("PlayerStatus");
        };
        PlayerStatusModel.prototype.parse = function (response) {
            return response;
        };
        return PlayerStatusModel;
    })(Backbone.Model);
    CCG.PlayerStatusModel = PlayerStatusModel;
})(CCG || (CCG = {}));
//@ sourceMappingURL=PlayerStatusModel.js.map
