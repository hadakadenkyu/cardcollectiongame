/// <reference path="../d.ts/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../d.ts/DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../d.ts/DefinitelyTyped/backbone/backbone.d.ts" />
/// <reference path="./view/AppView.ts" />
"use strict";
interface unitDataMaster{
	id:string;
	name:string;
	src:string;
	type:number;
	slot:number;
	maxLevel:number;
	initialLevel:number;
	maxHP:number;
	initialHP:number;
	maxLuck:number;
	initialLuck:number;
	maxSpeed:number;
	initialSpeed:number;
	maxAccuracy:number;
	initialAccuracy:number;
	maxStrike:number;
	initialStrike:number;
	maxSpecial:number;
	initialSpecial:number;
	maxProtection:number;
	initialProtection:number;
	maxInspiration:number;
	initialInspiration:number;
	maxAntiSpecial:number;
	initialAntiSpecial:number;
	slots:string[]; // 初期装備
	slot1:slotData;
	slot2:slotData;
	slot3:slotData;
	slot4:slotData;
	resource1:number;
	resource2:number;
	resource3:number;
	resource4:number;
	promoteTo?:string; // 進化先ID
	promotedFrom?:string; // 進化元ID
	promotionLevel?:number; // 進化レベル
	rarity:number;
}
// 武器スロットの設定
interface slotData{
	enabled:boolean; // 有効無効
	specialWeaponCapacity?:number; // 魔法アイテム搭載数
	capableTypes?:string[]; // スロットタイプ。適合しないと武器が入らない
}
interface activeUnitData{
	id:string;
	name:string;
	originalId:string;
	src:string;
	type:number;
	slot:number;
	level:number;
	HP:number;
	MP:number;
	luck:number;
	speed:number;
	accuracy:number;
	strike:number;
	protection:number;
	slots:string[];
	slot1:slotData;
	slot2:slotData;
	slot3:slotData;
	slot4:slotData;
	rarity:number;
}
interface weaponDataMaster{
	id:string;
	name:string;
	type:string;
	description?:string;
	speed:number;
	accuracy:number;
	special:number;
	strike:number;
	antiSpecial:number;
	inspiration:number;
	rarity:number;
}
interface playerStatus{
    level:number;
    exp:number;
    resource1:number;
    resource2:number;
    resource3:number;
    resource4:number;
    facility1:number;
    facility2:number;
    party:number;
}
// backbone.localstorage使う準備
declare module Backbone {
	var LocalStorage:any;
}
interface JQuery {
	jqMini:any;
}
declare var UUID;
module CCG{
	export var unitDataMaster:{[id:string]:unitDataMaster;};
	export var weaponDataMaster:{[id:string]:weaponDataMaster;}
	var history_go = window.history.go;
	var history_back = window.history.back;
	export var settings = {};
	export function disableHistoryBack(){
		// 履歴管理
		window.history.go = function(){};
		window.history.back = function(){};
	}
	export function enableHistoryBack(){
		window.history.go = history_go;
		window.history.back = history_back;
	}
	export function capitaliseFirstLetter(string){
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}
	export function toCamelCase(string){
	    return string.replace(/(\-([a-z]))/g, function($1,$2,$3){return $3.toUpperCase();});
	}
	export var appView;
	export function initialize($appElem){
		var jqmini = $appElem.jqMini({
		    transition: "none",
		    scrollCheck: false,
		    external: "external",
		    hash: false
		});
		var uuid:string;
		if(typeof(window.localStorage) !== 'object'){
			$('body').empty().html('local storage is required')
		} else {
			if(!(uuid=localStorage.getItem('uuid'))){
				uuid = UUID.generate();	
				localStorage.setItem('uuid',uuid);
			}
			appView = new CCG.AppView({el:$appElem.get(0),jqmini:jqmini,uuid:uuid});
		}
	}
}